<?php
/**
 * Route all http requests to relavent functions
 * unmatched routes will return a 404 not found
 *
 * Autoloads classes and sets custom error handler
 *
 * @author Robert Curran <robert.d.curran@googlemail.com>
 **/

require "vendor/autoload.php";
require_once "GenerateMarker.class.php";

/**
 * Start the slim application
 */
$app = new \Slim\Slim();

/**
 * Setup a custom error handler to catch and log exceptions then return 404
 */
$app->error(function(Exception $e) use ($app) {
    $msg =  $e->getMessage();
    error_log($msg);
    $app->notFound();
});

/**
 * Route to return a numbered map pin marker
 */
$app->get("/marker/:template/:fontColor/:number", function($template, $fontColor, $number) use ($app) {
    $app->contentType('image/png');
    try {
        GenerateMarker::instance($template, $fontColor, $number)->getMarker();
    }
    catch( Exception $e ) {
        $app->contentType('text/html');
        return $app->error($e);
    }
})->conditions(array(
    "template"=>"[\w]+",
    "fontColor"=>"[\w]+",
    "number"=>"[0-9]+",
));

$app->run();

?>
