<?php
/**
 * Class that generates and serves a png map maker pin image with a text label by generting, sizing and combining
 * a label image with a marker template
 *
 * Requires ImageMagick to be installed on the system running this code.
 *
 * @author Robert Curran <robert.d.curran@googlemail.com>
 */

class GenerateMarker
{
    /**
     * @var string  a string containg the name of the png template file
     */
    private $template;

    /**
     * @var string  a string containg the 6 digit hex number of the font colour
     */
    private $fontColor;

    /**
     * @var int     a integer to use as the marker label
     */
    private $number;

    /**
     * @var string  a string containing the output filename of the generated image
     */
    private $outName;

    /**
     * @var array   an array of data about the template used to size and position and numeric label
    */
    private $templateData;

    /**
     * @var array   an array mapping colour names to 6 digit hex numbers
     */
    private static $colors = array(
        "black" => "000000",
        "white" => "FFFFFF"
    );

    /**
     * Instantiate the marker generator object with template and label details and perform validation on data
     *
     * @param string    $template   a string containg the name of the png template file
     * @param string    $fontColor  a string containg the 6 digit hex number or name of the font colour
     * @param int       $number     a integer to use as the marker label
     */
    public function __construct($template, $fontColor, $number)
    {
        $this->template = $this->checkTemplate($template);
        $this->fontColor = $this->checkFontColor($fontColor);
        $this->number = $this->checkNumber($number);
        $this->templateData = $this->loadTemplateData($template);
        $this->validateTemplateData();
        $this->outName = $this->getOutFileName();
    }

    /**
     * Static function that returns a new instance of self
     *
     * @param string    $template   a string containg the name of the png template file
     * @param string    $fontColor  a string containg the 6 digit hex number or name of the font colour
     * @param int       $number     a integer to use as the marker label
     * @return GenerateMarker   instance of self
     */
    public static function instance($template, $fontColor, $number)
    {
        return new self($template, $fontColor, $number);
    }

    /**
     * Checks if requested marker already exists and either generates and serves or just serves the image
     */
    public function getMarker()
    {
        if (!file_exists(__DIR__ . "/output/" . $this->outName)) {
            $this->generateMarker();
        }

        header('Content-Type: image/png');
        readfile("/var/www/html/mapmarkers/output/$this->outName");
    }

    /**
     * Checks if the requested template exists
     *
     * @param  string    $template   a string containg the name of the png template file
     * @return string   the full filename of the tempalte to use
     * @throws Exception    for a missing tempalte file
     */
    private function checkTemplate($template)
    {
        if (!file_exists(__DIR__ . "/templates/$template.png")) {
            throw new Exception("Template not found: " . $template . ".png");
        }

        return "$template.png";
    }

    /**
     * Checks that requested font colour is valid 6 digit hex code or tries to convert it to one using predefined
     * mapping defined in this class
     *
     * @param  string   $color  a string containg the 6 digit hex number or name of the font colour
     * @return string   the validated font colour hex code
     * @throws  Exception   for an invalid font colour or name with no mapping
     */
    private function checkFontColor($color)
    {
        $color = strtolower($color);

        if (preg_match('/^([a-f0-9]{6})$/i', strtoupper($color), $match)) {
            return $match[0];
        }

        if (array_key_exists($color, self::$colors)) {
            return self::$colors[$color];
        }

        throw new Exception("Invalid font color: " . $color);
    }

    /**
     * Checks ths requested label number is valid
     *
     * @param  int  $number     the requested  integer to use as the marker label
     * @return int  the integer to use as the marker label
     */
    private function checkNumber($number)
    {
        return $number;
    }

    /**
     * Loads information about the current template from the template data json file
     *
     * @param  string   $template   a string containg the name of the png template file
     * @return array    an array of data about the template
     * @throws Exception    for a missing json file or missing template key in the parsed json
     */
    private function loadTemplateData($template)
    {
        $data = json_decode( file_get_contents(__DIR__ . "/templates/templateData.json"), true );

        if ($data) {
            $templateData = isset($data[$template]) ? $data[$template] : null;

            if (!empty($templateData)) {
                return $templateData;
            }
            throw new Exception("Unable to load template data for $template.");
        }

        throw new Exception("Unable to load template data file.");
    }

    /**
     * Validates an array of template data by checking certian keys are present
     *
     * @return bool validity of data
     * @throws Exception for missing keys
     */
    private function validateTemplateData()
    {
        $expectedKeys = array(
            "targetZoneWidth",
            "targetZoneHeight",
            "targetZoneXOffset",
            "targetZoneYOffset"
        );

        $actualKeys = array_keys($this->templateData);

        $diff = array_diff($expectedKeys, $actualKeys);

        if (!empty($diff)) {
            throw new Exception("Invalid template data, the following fields are missing: "
                . implode(", ", $diff) . ".");
        }

        return true;
    }

    /**
     * Determines the output filename based on the request paramerters
     *
     * @return string the output filename
     */
    private function getOutFileName()
    {
        return md5($this->template.$this->fontColor.$this->number) . ".png";
    }

    /**
     * Generates a new marker image by creating a label image and combining this with the template
     *
     * @return void
     */
    private function generateMarker()
    {
        $fontsize = 14;
        $now = time();
        $labelImagePath = "/var/www/html/mapmarkers/tmp/$now-$this->number.png";

        $this->makeNumberImage( $labelImagePath, $this->number, $fontsize, $this->fontColor);
        $imageSize = $this->getImageSize( $labelImagePath );

        if (!$this->checkLabelSize($this->templateData['targetZoneWidth'], $this->templateData['targetZoneHeight'],
        $imageSize['width'], $imageSize['height'])) {
            $this->resizeLabelImage( $labelImagePath, $this->templateData['targetZoneWidth'],
                $this->templateData['targetZoneHeight']);
            $imageSize = $this->getImageSize( $labelImagePath );
        }

        $this->combineTemplateAndText( $this->template, $labelImagePath, $imageSize['width'], $imageSize['height']);

        unlink($labelImagePath);
    }

    /**
     * Generates a new label image
     *
     * @param  string $outPath output filename
     * @param  string $text    label text
     * @param  string $size    fontsize for the text
     * @param  string $color   6 digit hex code for font colour
     * @return void
     * @throws Exception       on error generating the image
     */
    private function makeNumberImage($outPath, $text, $size, $color)
    {
        exec("convert -background transparent -fill \"#$color\" -font Helvetica -pointsize $size label:$text\
         -trim $outPath", $out, $ret);

        if( $ret !== 0 ){
            throw new Exception("Error generating label image.");
        }
    }

    /**
     * Determines the size (width and height) pf an image file
     *
     * @param  string $image full file path of the image to size
     * @return array         an array with width and height keys containg the pixel dimensions of the image
     * @throws Exception     on error getting image dimensions
     */
    private function getImageSize($image)
    {
        exec("identify -format \"%w,%h\" $image", $out, $ret);
        $dims = explode(",",$out[0]);

        if( $ret !== 0 ){
            throw new Exception("Error getting image size: " . $image);
        }

        return array(
            "width" => $dims[0],
            "height" => $dims[1]
        );
    }

    /**
     * Checks the size a set of width, height dimensions against a max allowed set
     *
     * @param  int $mW  max allowed width in pixels
     * @param  int $mH  max allowed height in pixels
     * @param  int $w   width in pixels
     * @param  int $h   height in pixels
     * @return bool     indicates if dimensions are within max allowed
     */
    private function checkLabelSize($maxW, $maxH, $w, $h)
    {
        return $w <= $maxW && $h <= $maxH;
    }

    /**
     * Resizes a label image to fit a max allowed width and height, aspect ration is maintianed
     *
     * @param  string $image     full file path of image to resize
     * @param  int    $maxWidth  max width in pixels
     * @param  int    $maxHeight max height in pixels
     * @return void
     * @throws Exception    on error resizing image
     */
    private function resizeLabelImage($image, $maxWidth, $maxHeight)
    {
        exec("convert $image -resize " . $maxWidth . "x" . $maxHeight . " " . $image, $out, $ret);

        if( $ret !== 0 ){
            throw new Exception("Error resizing image: " . $image);
        }
    }

    /**
     * Generates final output image by combining the template and lable images with the template data
     *
     * @param  string $templateImage template image filename
     * @param  string $textImage     label image full file path
     * @param  int $textWidth        label image width in pixels
     * @param  int $textHeight       label image height in pixels
     * @return void
     * @throws Exception             on error generating final image
     */
    private function combineTemplateAndText( $templateImage, $textImage, $textWidth, $textHeight )
    {
        $xOffset = intval((($this->templateData['targetZoneWidth']-$textWidth)/2)
            +$this->templateData['targetZoneXOffset']);
        $yOffset = intval((($this->templateData['targetZoneHeight']-$textHeight)/2)
            +$this->templateData['targetZoneYOffset']);

        $cmd = "convert /var/www/html/mapmarkers/templates/$templateImage -draw \"image Over $xOffset,$yOffset 0,0 '";
        $cmd .= $textImage . "'\" ";
        $cmd .= "/var/www/html/mapmarkers/output/" . $this->outName;

        exec($cmd, $out, $ret);

        if( $ret !== 0 ){
            throw new Exception("Error generating final marking image.");
        }
    }
}

?>
